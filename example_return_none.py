from typing import NoReturn


def play(player_name: str) -> None:
    print(f"player name: {player_name}")


def black_hole() -> NoReturn:
    raise Exception("There is no going back...")


if __name__ == "__main__":
    ret_val = play("Maria")
